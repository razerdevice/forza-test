import axios from 'axios';
import {API_URL, DEVICE_ID} from '../utils/constants';
import {safeCall} from '../utils/helpers';

const instance = axios.create({baseURL: API_URL});

async function getUser() {
  const response = await instance.post('/users', {
    device_id: DEVICE_ID,
  });
  return response?.data?.user_id;
}

async function getChallenges(userID) {
  const response = await instance.get(`/users/${userID}/challenges`);
  const [activeChallenges] = response?.data?.active_challenges || [];
  return activeChallenges || {};
}

async function makePrediction(userID, challangeId, predictionsData) {
  return await instance.post(`/users/${userID}/challenges/${challangeId}`, {
    predictions: predictionsData,
  });
}

export default {
  getUser: safeCall(getUser),
  getChallenges: safeCall(getChallenges),
  makePrediction: safeCall(makePrediction),
};
