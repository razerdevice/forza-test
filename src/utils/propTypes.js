import PropTypes from 'prop-types';

export const Prediction = PropTypes.shape({
  match_id: PropTypes.number.isRequired,
  prediction: PropTypes.string.isRequired,
});

export const Match = PropTypes.shape({
  away_team_name: PropTypes.string.isRequired,
  away_team_odds: PropTypes.number.isRequired,
  draw_odds: PropTypes.number.isRequired,
  home_team_name: PropTypes.string.isRequired,
  home_team_odds: PropTypes.number.isRequired,
  id: PropTypes.number.isRequired,
  starts_at: PropTypes.string.isRequired,
  tournament_name: PropTypes.string.isRequired,
});
