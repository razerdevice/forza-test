import {MAX_POSSIBLE_PREDICTIONS, MONTHS} from './constants';

export const safeCall =
  (fn) =>
  (...args) => {
    try {
      return fn(...args);
    } catch (e) {
      console.log(e);
    }
  };

const fillTime = (mins) => ('0' + mins).slice(-2);

export const parseDateTime = (dateStr) => {
  const d = new Date(dateStr);
  return `${MONTHS[d.getMonth()]} ${d.getDate()} / ${d.getHours()}:${fillTime(d.getMinutes())}`;
};

export const calcProgress = (currentStep) => {
  const completed = currentStep > 0 ? (currentStep / MAX_POSSIBLE_PREDICTIONS) : 0;
  return Math.min(Math.round(completed * 100) + 5, 100);
};

const formatDiff = (difference) => {
  const timeLeft = {
    hours: Math.floor(difference / (1000 * 60 * 60)),
    minutes: Math.floor((difference / 1000 / 60) % 60),
    seconds: Math.floor((difference / 1000) % 60),
  };
  return `${timeLeft.hours}:${fillTime(timeLeft.minutes)}:${fillTime(timeLeft.seconds)}`;
};

export const calculateTimeLeft = (eventTS) => {
  const difference = eventTS - new Date().getTime();
  return difference > 0 ? formatDiff(difference) : '0';
};
