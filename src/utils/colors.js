const COLORS = {
  white: '#FFFFFF',
  red: '#FD521F',
  lightRed: '#FEA88F',
  grey: '#BDBDBD',
  paleGrey: '#F2F2F2',
  dark: '#333333',
  green: '#6FCF97',
  navy: '#122336',
};

export default COLORS;
