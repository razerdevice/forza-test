export const API_URL = 'https://demo.forza-challenge.com/api/v1';

export const DEVICE_ID = '72fe37ee-62f5-444f-a237-dc00c195d1c5';

export const DRAW = 'draw';
export const HOME = 'home';
export const AWAY = 'away';

export const RESULT_TYPES_MAP = {
  [DRAW]: 'X',
  [HOME]: '1',
  [AWAY]: '2',
};

export const MAX_POSSIBLE_PREDICTIONS = 10;

export const MONTHS = [
  'Jan',
  'Feb',
  'Mar',
  'Apr',
  'May',
  'Jun',
  'Jul',
  'Aug',
  'Sep',
  'Oct',
  'Nov',
  'Dec',
];
