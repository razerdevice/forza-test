import styled from 'styled-components';

const AppText = styled.Text`
  font-family: Roboto-Regular;
`;

export default AppText;
