import React, {useState, useEffect} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import COLORS from '../../utils/colors';
import AppText from '../../common/AppText';
import {calculateTimeLeft} from '../../utils/helpers';

const CountdownInfo = ({statsAt}) => {
  const eventTS = new Date(statsAt).getTime();

  const [timeLeft, setTimeLeft] = useState(calculateTimeLeft(eventTS));

  useEffect(() => {
    const timer = setTimeout(() => {
      setTimeLeft(calculateTimeLeft(eventTS));
    }, 1000);

    return () => clearTimeout(timer);
  });

  return (
    <CountdownInfoContainer>
      <CountdownPill>
        <PillText>{timeLeft}</PillText>
      </CountdownPill>
    </CountdownInfoContainer>
  );
};

const CountdownInfoContainer = styled.View`
  flex-direction: row;
  margin-bottom: 12px;
  justify-content: flex-end;
  margin: 5px 12px 12px 5px;
`;

const CountdownPill = styled.View`
  width: 70px;
  height: 21px;
  border-radius: 9px;
  background-color: ${COLORS.red};
  border: 2px solid ${COLORS.white};
  box-shadow: 0px 0px 2px rgba(0, 0, 0, 0.5);
  align-items: center;
  justify-content: center;
`;

const PillText = styled(AppText)`
  font-weight: 500;
  font-size: 12px;
  color: ${COLORS.white};
`;

CountdownInfo.propTypes = {
  statsAt: PropTypes.string.isRequired,
};

export default CountdownInfo;
