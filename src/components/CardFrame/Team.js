import React from 'react';
import PropTypes from 'prop-types';
import {StyleSheet} from 'react-native';
import styled from 'styled-components';
import COLORS from '../../utils/colors';

const Team = (props) => {
  return (
    <TeamCard>
      <TeamBackground
        source={props.background}
        imageStyle={props.isLeft ? leftTeamBorder : rightTeamBorder}>
        <TeamName>{props.name}</TeamName>
      </TeamBackground>
    </TeamCard>
  );
};

const TeamCard = styled.View`
  flex: 1;
  height: 100%;
`;

const TeamBackground = styled.ImageBackground`
  flex: 1;
  resize-mode: cover;
  align-items: center;
  justify-content: center;
`;

const TeamName = styled.Text`
  font-family: 'RobotoCondensed-Regular';
  font-weight: 700;
  font-size: 32px;
  color: ${COLORS.white};
  margin-top: -20px;
`;

const leftTeamBorder = StyleSheet.create({
  borderTopLeftRadius: 6,
  borderBottomLeftRadius: 6,
});

const rightTeamBorder = StyleSheet.create({
  borderTopRightRadius: 6,
  borderBottomRightRadius: 6,
});

Team.propTypes = {
  background: PropTypes.any,
  isLeft: PropTypes.bool,
  name: PropTypes.string,
};

export default Team;
