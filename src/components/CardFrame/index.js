import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Card from './Card';
import CountdownInfo from './CountdownInfo';
import COLORS from '../../utils/colors';
import {Match} from '../../utils/propTypes';

const CardFrame = ({
  makePrediction,
  currentMatch,
  currentMatches,
  currentStep,
}) => {
  if (!currentMatch) {
    return null;
  }
  return (
    <CardFrameContainer>
      <CountdownInfo statsAt={currentMatch.starts_at} />
      <Card
        currentMatches={currentMatches}
        currentStep={currentStep}
        makePrediction={makePrediction}
        currentMatch={currentMatch}
      />
    </CardFrameContainer>
  );
};

const CardFrameContainer = styled.View`
  flex-direction: column;
  flex: 1;
  width: 100%;
  border-top-right-radius: 4px;
  border-top-left-radius: 4px;
  background-color: ${COLORS.white};
  padding: 10px 4px 4px 4px;
`;

CardFrame.propTypes = {
  makePrediction: PropTypes.func.isRequired,
  currentStep: PropTypes.number.isRequired,
  currentMatch: PropTypes.object,
  currentMatches: PropTypes.arrayOf(Match),
};

export default CardFrame;
