import React from 'react';
import PropTypes from 'prop-types';
import {TouchableOpacity} from 'react-native';
import styled from 'styled-components';
import COLORS from '../../utils/colors';

const CardButton = ({onPress, title, coefficient, size = 72}) => {
  return (
    <TouchableOpacity onPress={onPress} activeOpacity={0.2}>
      <ButtonContainer size={size}>
        <InnerCircle size={size}>
          <BtnTitle>
            {title}
          </BtnTitle>
          <BtnText>
            {coefficient}
          </BtnText>
        </InnerCircle>
      </ButtonContainer>
    </TouchableOpacity>
  );
};

const ButtonContainer = styled.View`
  height: ${({size}) => `${size}px`};
  width: ${({size}) => `${size}px`};
  border-radius: 36px;
  background-color: ${COLORS.white};
  align-items: center;
  justify-content: center;
  margin-horizontal: 8px;
`;

const InnerCircle = styled.View`
  height: ${({size}) => `${size * 0.8}px`};
  width: ${({size}) => `${size * 0.8}px`};
  border-radius: 36px;
  box-shadow: 0px 1.44px 2.88px rgba(0, 0, 0, 0.35);
  background-color: ${COLORS.white};
  align-items: center;
  justify-content: center;
`;

const BtnText = styled.Text`
  font-family: Roboto-Regular;
  font-size: 11px;
  color: ${COLORS.navy};
  font-weight: 400;
  text-align: center;
`;

const BtnTitle = styled.Text`
  font-family: RobotoCondensed-Regular;
  font-weight: 700;
  font-size: 13px;
`;

CardButton.propTypes = {
  size: PropTypes.number,
  onPress: PropTypes.func,
  title: PropTypes.string,
  coefficient: PropTypes.number,
};

export default CardButton;
