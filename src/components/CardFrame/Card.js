import React from 'react';
import Swiper from 'react-native-deck-swiper';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import teamA from '../../../assets/images/team-a.png';
import teamB from '../../../assets/images/team-b.png';
import Team from './Team';
import CardButton from './CardButton';
import {DRAW, HOME, AWAY} from '../../utils/constants';
import AppText from '../../common/AppText';
import COLORS from '../../utils/colors';
import {parseDateTime} from '../../utils/helpers';
import {Match} from '../../utils/propTypes';

const Card = ({
  currentMatch = {},
  makePrediction,
  currentStep,
  currentMatches,
}) => {
  const {
    home_team_name,
    away_team_name,
    tournament_name,
    id,
    away_team_odds,
    draw_odds,
    home_team_odds,
    starts_at,
  } = currentMatch;

  const teams = [
    {
      background: teamA,
      name: home_team_name,
    },
    {
      background: teamB,
      name: away_team_name,
    },
  ];
  const buttonsProps = [
    {
      type: HOME,
      title: 'Home',
      coefficient: home_team_odds,
    },
    {
      type: DRAW,
      title: 'Draw',
      coefficient: draw_odds,
      size: 64,
    },
    {
      type: AWAY,
      title: 'Away',
      coefficient: away_team_odds,
    },
  ];
  return (
    <SwiperContainer>
      <Swiper
        cards={currentMatches}
        cardVerticalMargin={0}
        cardHorizontalMargin={0}
        renderCard={(card) => {
          return (
            <CardContainer>
              {teams.map((team, index) => (
                <Team {...team} key={index} isLeft={index === 0} />
              ))}
              <ButtonsGroup>
                {buttonsProps.map((btnProps) => (
                  <CardButton
                    {...btnProps}
                    key={btnProps.title}
                    onPress={() => makePrediction(id, btnProps.type)}
                  />
                ))}
              </ButtonsGroup>
              <TournamentName>{tournament_name}</TournamentName>
              <MatchTime>{parseDateTime(starts_at)}</MatchTime>
            </CardContainer>
          );
        }}
        onSwipedLeft={() => makePrediction(id, HOME)}
        onSwipedRight={() => makePrediction(id, AWAY)}
        onSwipedBottom={() => makePrediction(id, DRAW)}
        cardIndex={currentStep}
        stackSize={4}
        disableTopSwipe={true}
        backgroundColor="transparent"
      />
    </SwiperContainer>
  );
};

const CardContainer = styled.View`
  width: 93%;
  flex: 0.74;
  border-radius: 8px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  position: relative;
  background-color: ${COLORS.white};
`;

const SwiperContainer = styled.View`
  width: 100%;
  flex: 1;
  border-radius: 8px;
`;

const ButtonsGroup = styled.View`
  position: absolute;
  bottom: 20px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const CommonText = styled(AppText)`
  position: absolute;
  font-size: 16px;
  position: absolute;
  top: 20px;
  font-weight: 700;
  color: ${COLORS.white};
`;

const TournamentName = styled(CommonText)`
  left: 15px;
  top: 15px;
  width: 45%;
  flex-wrap: wrap;
`;

const MatchTime = styled(CommonText)`
  right: 15px;
  top: 15px;
`;

Card.propTypes = {
  makePrediction: PropTypes.func.isRequired,
  currentStep: PropTypes.number.isRequired,
  currentMatch: PropTypes.object.isRequired,
  currentMatches: PropTypes.arrayOf(Match),
};

export default Card;
