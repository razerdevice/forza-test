import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import PredictionProgress from './PredictionProgress';
import Coin from './Coin';
import COLORS from '../../utils/colors';
import ProgressLine from './ProgressLine';
import {MAX_POSSIBLE_PREDICTIONS} from '../../utils/constants';
import {Prediction} from '../../utils/propTypes';

const StatusBar = ({predictions, currentStep}) => {
  return (
    <Wrapper>
      <StatusBarContainer>
        <PredictionProgress
          predictions={[...Array(MAX_POSSIBLE_PREDICTIONS)].map(
            (_, i) => predictions[i],
          )}
          currentStep={currentStep}
        />
        <Coin amount={currentStep} />
      </StatusBarContainer>
      <ProgressLine currentStep={currentStep} />
    </Wrapper>
  );
};

StatusBar.propTypes = {
  currentStep: PropTypes.number.isRequired,
  predictions: PropTypes.arrayOf(Prediction),
};

const StatusBarContainer = styled.View`
  height: 50px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 12px;
  background-color: ${COLORS.white};
`;

const Wrapper = styled.View`
  margin-bottom: 10px;
  height: 60px;
`;

export default StatusBar;
