import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import COLORS from '../../utils/colors';
import {calcProgress} from '../../utils/helpers';

const ProgressLine = ({currentStep}) => {
  return (
    <Container>
      <ActiveLine currentStep={currentStep} />
    </Container>
  );
};

const Container = styled.View`
  height: 3px;
  position: relative;
`;

const ActiveLine = styled.View`
  background-color: ${COLORS.green};
  height: 3px;
  width: ${({currentStep}) => `${calcProgress(currentStep)}%`};
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
`;

ProgressLine.propTypes = {
  currentStep: PropTypes.number.isRequired,
};

export default ProgressLine;
