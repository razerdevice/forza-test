import React from 'react';
import PredictionStep from './PredictionStep';
import styled from 'styled-components';
import PropTypes from 'prop-types';

const PredictionProgress = ({predictions, currentStep}) => (
  <PredictionProgressRow>
    {predictions.map((item, index) => (
      <PredictionStep
        key={index}
        {...item}
        isActive={index === currentStep}
        isComplete={index < currentStep}
      />
    ))}
  </PredictionProgressRow>
);

const PredictionProgressRow = styled.View`
  flex-direction: row;
`;

PredictionProgress.propTypes = {
  currentStep: PropTypes.number.isRequired,
  predictions: PropTypes.arrayOf(PropTypes.any),
};

export default PredictionProgress;
