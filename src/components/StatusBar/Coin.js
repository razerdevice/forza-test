import React from 'react';
import styled from 'styled-components';
import COLORS from '../../utils/colors';
import coin from '../../../assets/images/coin-large.png';
import PropTypes from 'prop-types';

const Coin = ({amount}) => {
  return (
    <CoinShape>
      <CoinImage source={coin} />
      <CoinNumber>
        {amount}
      </CoinNumber>
    </CoinShape>
  );
};

const CoinShape = styled.View`
  height: 19px;
  width: 41px;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  border: 2px solid ${COLORS.white};
  box-shadow: 0px 0.25px 1px rgba(0, 0, 0, 0.5);
  border-radius: 15px;
  background-color: ${COLORS.white};
  position: relative;
`;

const CoinImage = styled.Image`
  height: 24px;
  width: 24px;
  position: absolute;
  left: -4px;
  top: -5px;
`;

const CoinNumber = styled.Text`
  color: ${COLORS.dark};
  font-size: 16px;
  font-weight: 900;
  font-family: 'RobotoCondensed-Regular';
  position: absolute;
  right: 1px;
  top: -2px;
`;

Coin.propTypes = {
  amount: PropTypes.number.isRequired,
};

export default Coin;
