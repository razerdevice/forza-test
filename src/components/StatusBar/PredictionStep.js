import React from 'react';
import styled from 'styled-components';
import COLORS from '../../utils/colors';
import {RESULT_TYPES_MAP} from '../../utils/constants';
import PropTypes from 'prop-types';

const PredictionStep = ({prediction, isActive, isComplete}) => {
  return (
    <StepContainer isActive={isActive} isComplete={isComplete}>
      <StepText>
        {RESULT_TYPES_MAP[prediction]}
      </StepText>
    </StepContainer>
  );
};

const StepContainer = styled.View`
  width: 21px;
  height: 21px;
  border-radius: 4px;
  border: 2px solid;
  border-color: ${({isActive}) => (isActive ? COLORS.red : COLORS.white)}
  margin-right: 4px;
  justify-content: center;
  align-items: center;
  background-color: ${({isActive, isComplete}) =>
    isComplete ? COLORS.green : isActive ? COLORS.lightRed : COLORS.grey};
  box-shadow: 0px 0px 4px rgba(0, 0, 0, 0.25);
`;

const StepText = styled.Text`
  color: ${COLORS.white};
  font-size: 11px;
  font-weight: 700;
  font-family: 'RobotoCondensed-Regular';
`;

PredictionStep.propTypes = {
  isActive: PropTypes.bool.isRequired,
  isComplete: PropTypes.bool.isRequired,
  prediction: PropTypes.any,
};

export default PredictionStep;
