import React, {useEffect, useState} from 'react';
import {ActivityIndicator} from 'react-native';
import styled from 'styled-components';
import StatusBar from '../components/StatusBar';
import CardFrame from '../components/CardFrame';
import COLORS from '../utils/colors';
import API from '../api/API';
import {MAX_POSSIBLE_PREDICTIONS} from '../utils/constants';
import AppText from '../common/AppText';

const PredictionsScreen = () => {
  const [tries, setNextTry] = useState(0);

  const [user, setUser] = useState({});
  const [challenge, setChallenge] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  const [currentMatches, setMatches] = useState([]);
  const [predictions, setPrediction] = useState([]);

  useEffect(() => {
    async function fetchData() {
      setIsLoading(true);
      const userID = await API.getUser();
      setUser(userID);
      const {id, matches} = await API.getChallenges(userID);
      if (id && matches) {
        setMatches(matches);
        setChallenge(id);
        setIsLoading(false);
      }
    }
    fetchData();
  }, [tries]);

  const makePrediction = (matchID, predictedResult) => {
    const updatedPredictions = [
      ...predictions,
      {
        match_id: matchID,
        prediction: predictedResult,
      },
    ];
    setPrediction(updatedPredictions);
    if (updatedPredictions.length === MAX_POSSIBLE_PREDICTIONS) {
      submitPredictions(updatedPredictions);
    }
  };

  const submitPredictions = async (updatedPredictions) => {
    setIsLoading(true);
    await API.makePrediction(user, challenge, updatedPredictions);
    setIsLoading(false);
  };

  const resetApp = () => {
    setPrediction([]);
    setNextTry(tries + 1);
  };

  const currentStep = predictions.length;
  return (
    <PredictionsContainer>
      {isLoading ? (
        <Centered>
          <ActivityIndicator size="large" color={COLORS.grey} />
        </Centered>
      ) : (
        <>
          {currentStep === MAX_POSSIBLE_PREDICTIONS && (
            <Centered>
              <StartAgain onPress={() => resetApp()}>
                <StartAgainText>Great Game!</StartAgainText>
                <StartAgainTextBot>Start Again?</StartAgainTextBot>
              </StartAgain>
            </Centered>
          )}
          <StatusBar
            predictions={predictions}
            currentStep={currentStep}
          />
          <CardFrame
            makePrediction={makePrediction}
            currentMatches={currentMatches}
            currentStep={currentStep}
            currentMatch={currentMatches[currentStep]}
          />
        </>
      )}
    </PredictionsContainer>
  );
};

const PredictionsContainer = styled.View`
  display: flex;
  flex-direction: column;
  width: 100%;
  flex: 1;
  padding: 10px;
  background-color: ${COLORS.paleGrey};
  position: relative;
`;

const Centered = styled.View`
  display: flex;
  justify-content: center;
  align-items: center;
  flex: 1;
  position: absolute;
  left: 0px;
  right: 0px;
  top: 0px;
  bottom: 0px;
  background-color: rgba(0, 0, 0, 0.1);
`;

const StartAgain = styled.TouchableOpacity`
  height: 40px;
  font-size: 16px;
`;

const StartAgainText = styled(AppText)`
  font-size: 18px;
  font-weight: 400;
  margin-bottom: 10px;
`;

const StartAgainTextBot = styled(StartAgainText)`
  color: ${COLORS.green};
`;

export default PredictionsScreen;
