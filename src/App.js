import React from 'react';
import PredictionsScreen from './screens/PredictionsScreen';
import styled from 'styled-components';
import COLORS from './utils/colors';

const App = () => {
  return (
    <AppView>
      <StyledSafeArea>
        <PredictionsScreen />
      </StyledSafeArea>
    </AppView>
  );
};

const AppView = styled.View`
  display: flex;
  flex-direction: column;
  width: 100%;
  flex: 1;
  height: 100%;
  background-color: ${COLORS.paleGrey};
`;

const StyledSafeArea = styled.SafeAreaView`
  flex: 1;
`;

export default App;
